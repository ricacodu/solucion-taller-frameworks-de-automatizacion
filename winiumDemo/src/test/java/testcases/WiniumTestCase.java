package testcases;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

public class WiniumTestCase {

	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		
		DesktopOptions option = new DesktopOptions();
		
		//para ejecutar el driver
		//System.setProperty("webdriver.winium.desktop.driver.bin", "C:\\Users\\rcorralesd\\eclipse-workspace\\winiumDemo\\src\\test\\resources\\drivers\\winiumdriver\\Winium.Desktop.Driver.exe");
		
		option.setApplicationPath("C:\\Windows\\System32\\calc.exe");
		
		/*Le envio la ruta donde esta el ejecutable de la aplicación a automatizar (option) y el puerto
		por donde de esta ejecutando el driver en la URL ( este debe estar corriendo)*/
		
		WiniumDriver driver = new WiniumDriver(new URL("http://localhost:9999"), option);
		
		Thread.sleep(5000);
		
		driver.findElement(By.name("Siete")).click();
		driver.findElement(By.id("plusButton")).click();
		driver.findElement(By.name("Ocho")).click();
		driver.findElement(By.name("Es igual a")).click();
		
	
		Thread.sleep(5000);
		
		String output = driver.findElement(By.id("CalculatorResults")).getAttribute("Name");
		System.out.println(output);
		
		Thread.sleep(3000);
		
		driver.findElementById("Close");
		

	}

}
