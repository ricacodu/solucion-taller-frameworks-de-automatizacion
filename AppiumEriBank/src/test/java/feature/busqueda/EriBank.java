package feature.busqueda;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import task.Login;

import static net.serenitybdd.screenplay.GivenWhenThen.*;

@RunWith(SerenityRunner.class)
public class EriBank {
	Actor ricardo = Actor.named("Ricardo");
	
	 @Managed(driver = "appium")
	    public WebDriver driver;
	 
	 
	  @Before
	  public void danielCanDriver() {
		  ricardo.can(BrowseTheWeb.with(driver));
	   }
	  
	  @Test
	  public void search_result() {
		  when(ricardo).attemptsTo(Login.theApp());		  
	  }  

}
