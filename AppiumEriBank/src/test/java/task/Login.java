package task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import ui.LoginPage;

public class Login implements Task{

	@Override
	public <T extends Actor> void performAs(T actor) {
		// TODO Auto-generated method stub
		actor.attemptsTo(Enter.theValue("company").into(LoginPage.USER));
		actor.attemptsTo(Enter.theValue("company").into(LoginPage.PASSWORD));
		actor.attemptsTo(Click.on(LoginPage.BUTTON_LOGIN));
	}

	public static Login theApp() {
		return instrumented(Login.class);
	}


}
