package ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class LoginPage extends PageObject {

	 public static Target USER = Target.the("Field to enter the user").located(By.id("com.experitest.ExperiBank:id/usernameTextField"));
	 public static Target PASSWORD = Target.the("Field to enter the password").located(By.id("com.experitest.ExperiBank:id/passwordTextField")); 
	 public static Target BUTTON_LOGIN = Target.the("Button to enter the application").locatedBy("com.experitest.ExperiBank:id/loginButton");
	 

}
