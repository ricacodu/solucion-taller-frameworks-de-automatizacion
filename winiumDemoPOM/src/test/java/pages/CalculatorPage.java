package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;

public class CalculatorPage {
	
	private static WebElement element = null;
	
	public static WebElement boton_siete(WiniumDriver driver){
		
		element = driver.findElement(By.name("Siete"));
		return element;
	}
	
	
    public static WebElement boton_mas(WiniumDriver driver){
		
		element = driver.findElement(By.id("plusButton"));
		return element;
	}

    
    public static WebElement boton_ocho(WiniumDriver driver){
		
		element = driver.findElement(By.name("Ocho"));
		return element;
	}
    
    
    public static WebElement boton_igual(WiniumDriver driver){
		
		element = driver.findElement(By.name("Es igual a"));
		return element;
	}
    
    
    public static WebElement resultado(WiniumDriver driver){
		
		element = driver.findElement(By.id("CalculatorResults"));
		return element;
	}
    
    
    public static WebElement boton_cerrar(WiniumDriver driver){
		
		element = driver.findElementById("Close");
		return element;
	}
       
    
}
