package testcases;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

import pages.CalculatorPage;

public class CalculatorTest {

	private static WiniumDriver driver = null;
	
	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		
             operation();
	}
	
	
	public static void operation() throws InterruptedException, MalformedURLException{
		
		     DesktopOptions option = new DesktopOptions();
			
			option.setApplicationPath("C:\\Windows\\System32\\calc.exe");
			
			driver = new WiniumDriver(new URL("http://localhost:9999"), option);
			
			Thread.sleep(2000);
					
			CalculatorPage.boton_siete(driver).click();
			CalculatorPage.boton_mas(driver).click();
			CalculatorPage.boton_ocho(driver).click();
			CalculatorPage.boton_igual(driver).click();
				
			Thread.sleep(2000);
			
			String output = CalculatorPage.resultado(driver).getAttribute("Name");
			System.out.println(output);
			
			Thread.sleep(2000);
			
			CalculatorPage.boton_cerrar(driver).click();
	}

}
