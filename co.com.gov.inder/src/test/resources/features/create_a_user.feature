#Author: rcorralesd@bancolombia.com.co

@CreateACompany
Feature: Create a user in the Inder application
  I as a user of the inder application
  I need to register
  to access the other functions in the application

  @HappyCase
  Scenario: Create a user in the Inder application successfully
    Given the functionality to register
    When the user fill all these required fields and save the information
    |kindOfPerson    |idType              |identificationNumber|name  |lastName    |sex      |birthdate |key       |confirmKey |municipality|district |address|stratum|email            |telephone|
    |Persona Natural |Cédula de Ciudadanía|1035590270          |Andres|Gutierrez   |Masculino|10/12/1985|123*MM78s |123*MM78s  |Envigado    |El Salado|Avenida|4      |correo@gmail.com |2225555  |
    Then the user will have created an account in the application
    |message |
    |Andres  |
    
#El name del WHEN y el message del THEN deben ser iguales. El xpath de ConfirmationSavedDataPage tambíen se debe modificar si los anteriores se cambian.