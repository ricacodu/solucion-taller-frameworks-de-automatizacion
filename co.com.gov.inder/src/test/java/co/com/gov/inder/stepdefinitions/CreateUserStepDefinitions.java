package co.com.gov.inder.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.gov.inder.model.ApplicationForm;
import co.com.gov.inder.model.VerificationMessage;
import co.com.gov.inder.questions.TheData;
import co.com.gov.inder.tasks.CreateAnAccount;
import co.com.gov.inder.tasks.OpenTheSite;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class CreateUserStepDefinitions {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor daniela = Actor.named("Daniela");
	
	@Before
	public void configuracionInicial() {
		daniela.can(BrowseTheWeb.with(hisBrowser));
	}
	
	
	@Given("^the functionality to register$")
	public void theFunctionalityToRegister() throws Exception {
		daniela.wasAbleTo(OpenTheSite.ofTheInder());
	}

	@When("^the user fill all these required fields and save the information$")
	public void theUserFillAllTheseRequiredFieldsAndSaveTheInformation(List<ApplicationForm> applicationForm) throws Exception {
		daniela.attemptsTo(CreateAnAccount.inTheApplication(applicationForm));
	}

	@Then("^the user will have created an account in the application$")
	public void theUserWillHaveCreatedAnAccountInTheApplication(List<VerificationMessage> verificationMessage) throws Exception {
		daniela.should(seeThat(TheData.wasSaved(), equalTo(verificationMessage.get(0).getMessage())));
	}
}
