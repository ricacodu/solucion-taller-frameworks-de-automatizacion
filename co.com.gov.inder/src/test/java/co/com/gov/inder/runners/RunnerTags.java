package co.com.gov.inder.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/create_a_user.feature",
		tags= "@HappyCase",
		glue="co.com.gov.inder.stepdefinitions",
		snippets=SnippetType.CAMELCASE		)
public class RunnerTags {

}
