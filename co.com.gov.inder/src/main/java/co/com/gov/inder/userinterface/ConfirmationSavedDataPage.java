package co.com.gov.inder.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class ConfirmationSavedDataPage {
	
	public static final Target MESSAGE_CONFIRMATION = Target.the("Message of confirmation data saved")
            .located(By.xpath("//span[contains(text(),'Andres')]"));
	
	
}
