package co.com.gov.inder.tasks;

import java.util.List;

import org.openqa.selenium.Keys;

import co.com.gov.inder.model.ApplicationForm;
import static co.com.gov.inder.userinterface.InderRegisterPage.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;


public class CreateAnAccount implements Task {

     private List<ApplicationForm> applicationForm;
	
	public CreateAnAccount(List<ApplicationForm> applicationForm) {
		super();
		this.applicationForm = applicationForm;
	}
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		 
		actor.attemptsTo(Click.on(KIND_OF_PERSON));
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getKindOfPerson()).into(KIND_OF_PERSON_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(KIND_OF_PERSON_SEARCH)); 
		
		actor.attemptsTo(Click.on(ID_TYPE));
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getIdType()).into(ID_TYPE_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(ID_TYPE_SEARCH));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getIdentificationNumber()).into(IDENTIFICATION_NUMBER));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getName()).into(NAME));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getLastName()).into(LAST_NAME));
		
		actor.attemptsTo(Click.on(GENDER));
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getSex()).into(GENDER_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(GENDER_SEARCH));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getBirthdate()).into(BIRTH_DATE));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getKey()).into(KEY));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getConfirmKey()).into(CONFIRM_KEY));
		
		actor.attemptsTo(Click.on(MUNICIPALITY));
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getMunicipality()).into(MUNICIPALITY_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(MUNICIPALITY_SEARCH));
		
		actor.attemptsTo(Click.on(DISTRICT));
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getDistrict()).into(DISTRICT_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(DISTRICT_SEARCH));
		
		actor.attemptsTo(Click.on(ADDRESS));
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getAddress()).into(ADDRESS_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(ADDRESS_SEARCH));
		
		actor.attemptsTo(Click.on(STRATUM));
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getStratum()).into(STRATUM_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(STRATUM_SEARCH));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getEmail()).into(EMAIL));
		
		actor.attemptsTo(Enter.theValue(applicationForm.get(0).getTelephone()).into(TELEPHONE));
		
		actor.attemptsTo(Click.on(HABEAS_DATA));
		
		actor.attemptsTo(Click.on(ACCEPT_POLICIES));
		
		actor.attemptsTo(Click.on(SAVE_BUTTOM));
		
}

	
	public static Performable inTheApplication(List<ApplicationForm> applicationForm) {
		return Tasks.instrumented(CreateAnAccount.class, applicationForm );
	}

	
}
