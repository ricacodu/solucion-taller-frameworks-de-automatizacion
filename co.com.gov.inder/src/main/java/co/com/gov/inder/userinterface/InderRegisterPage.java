package co.com.gov.inder.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://simon.inder.gov.co/registro")
public class InderRegisterPage extends PageObject{

	public static final Target KIND_OF_PERSON = Target.the("Type of person")
            .located(By.id("select2-chosen-15"));
	
	public static final Target KIND_OF_PERSON_SEARCH = Target.the("Type of person to choose")
            .located(By.id("s2id_autogen15_search"));
	
	public static final Target ID_TYPE = Target.the("Type of identificaction")
            .located(By.id("select2-chosen-16"));
	
	public static final Target ID_TYPE_SEARCH = Target.the("Type of identification to choose")
            .located(By.id("s2id_autogen16_search"));
	
	public static final Target IDENTIFICATION_NUMBER = Target.the("Identification number of the user")
            .located(By.id("numeroidentificacion"));
	
	public static final Target NAME = Target.the("Name of user")
            .located(By.id("nombres"));
	
	public static final Target LAST_NAME = Target.the("Last name of the user")
            .located(By.id("apellidos"));
	
	public static final Target GENDER = Target.the("Gender of user")
            .located(By.id("select2-chosen-17"));
	
	public static final Target GENDER_SEARCH = Target.the("Type of gender to choose")
            .located(By.id("s2id_autogen17_search"));
	
	public static final Target BIRTH_DATE = Target.the("Birthdate of the user")
            .located(By.id("fechanacimiento"));
	
	public static final Target KEY = Target.the("Key of the user")
            .located(By.id("clave_uno"));
	
	public static final Target CONFIRM_KEY = Target.the("Confirm Key of the user")
            .located(By.id("clave_dos"));
	
	public static final Target MUNICIPALITY = Target.the("Type of municipality")
            .located(By.id("select2-chosen-19"));
	
	public static final Target MUNICIPALITY_SEARCH = Target.the("Type of municipality to choose")
            .located(By.id("s2id_autogen19_search"));
	
	public static final Target DISTRICT = Target.the("Type of district")
            .located(By.id("select2-chosen-20"));
	
	public static final Target DISTRICT_SEARCH = Target.the("Type of district to choose")
            .located(By.id("s2id_autogen20_search"));
	
	public static final Target ADDRESS = Target.the("Address of the user")
            .located(By.id("select2-chosen-22"));
	
	public static final Target ADDRESS_SEARCH = Target.the("Type of address to choose")
            .located(By.id("s2id_autogen22_search"));
	
	public static final Target STRATUM = Target.the("Stratum of the user")
            .located(By.id("select2-chosen-28"));
	
	public static final Target STRATUM_SEARCH = Target.the("Number of stratum to choose")
            .located(By.id("s2id_autogen28_search"));
	
	public static final Target EMAIL = Target.the("Email of the user")
            .located(By.id("correoelectronico"));
	
	public static final Target TELEPHONE = Target.the("Telephone of the user")
            .located(By.id("telefonomovil"));
	
	public static final Target HABEAS_DATA = Target.the("Check Habeas Data")
            .located(By.className("icheckbox_square-blue"));
	
	public static final Target ACCEPT_POLICIES = Target.the("Check to accept policies")
            .located(By.xpath("//div[@class='infoBox row']//div[7]//div[1]"));
	
	public static final Target SAVE_BUTTOM = Target.the("Buttom save info")
            .located(By.id("registro_save"));	
	
}
