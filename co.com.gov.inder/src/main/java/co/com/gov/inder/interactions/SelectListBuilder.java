package co.com.gov.inder.interactions;

import net.serenitybdd.screenplay.targets.Target;

public class SelectListBuilder {

	private Target list;
	
	public SelectListBuilder(Target list) {
		this.list=list;
	}

	public Selection theOption(String option)
	{
		return new Selection(list, option);
		
	}
	
}
