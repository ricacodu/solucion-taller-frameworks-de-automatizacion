package co.com.gov.inder.tasks;

import co.com.gov.inder.userinterface.InderRegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class OpenTheSite implements Task {

	private InderRegisterPage inderRegisterPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(inderRegisterPage));
		
	}
	
    public static OpenTheSite ofTheInder() {
		
		return Tasks.instrumented(OpenTheSite.class);
	}

}
