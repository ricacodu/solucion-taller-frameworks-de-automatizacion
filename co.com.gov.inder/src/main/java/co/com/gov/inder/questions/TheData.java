package co.com.gov.inder.questions;

import co.com.gov.inder.userinterface.ConfirmationSavedDataPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class TheData implements Question<String> {

	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(ConfirmationSavedDataPage.MESSAGE_CONFIRMATION).viewedBy(actor).asString();
	}

	public static TheData wasSaved() {
	return new TheData();
	}

}
